import pickle
import pandas as pd
import os
from bs4 import BeautifulSoup
import spacy
import unicodedata
import fasttext
from scipy import spatial


def get_cases_and_laws_formatted(doc):
      # Load saved models
      case_and_law_model = spacy.load('en_lawpavilion_ner')

      soup = BeautifulSoup(doc)
      for script in soup(["script", "style"]):
            script.extract()
      text = soup.get_text()

      normalized_res = set()
      res = case_and_law_model(text).ents
      for r in res:
            normalized_res.add(unicodedata.normalize('NFKD', str(r).lower().replace(' vs ','v.').replace(' v ', 'v.').replace('(', '').replace(')', '').replace('-', '')).replace(' ', ''))
      return normalized_res



# Get the top 10 most similar documents
def get_topsim(content, full_text, vector_list, model_file, merged_dataset):
      # Word tokenize
      model = fasttext.load_model(model_file)

      # Get cases and laws in content text
      content_cases_and_laws = get_cases_and_laws_formatted(full_text)

      # Get all document vectors
      with open(vector_list, 'rb') as f:
            vecs_list = pickle.load(f)      

      # Get vector from model and check similarity with each vector
      doc_vec = model.get_sentence_vector(content)
      all_sims = [(idx, 1 - spatial.distance.cosine(doc_vec, cur_vec)) for idx, cur_vec in enumerate(vecs_list)]
      all_sims.sort(key=lambda x: -x[1])

      sims = all_sims[:10]

      top_sim = []
      for simdoc in sims:
            simdoc_cases_and_laws = eval(merged_dataset.iloc[simdoc[0]]["STRIPPED CASES AND LAWS"])
            top_sim.append((simdoc[0], simdoc[1], len(content_cases_and_laws.intersection(simdoc_cases_and_laws))))

      top_sim.sort(key=lambda x: (-x[2], -x[1]))
      return top_sim



def get_similar(content, full_text, vector_list, model_file, PATH_TO_DATASET):
      # Read dataset from path
      merged_dataset = pd.read_csv(os.path.join(PATH_TO_DATASET, 'Merged_Dataset_Standardized_V3.csv'))

      # Get the result from get_topsim function
      result = get_topsim(content = content['content'], vector_list = vector_list, full_text = full_text, model_file = model_file, merged_dataset=merged_dataset)

      results = []

      courts = {'CA': 'Court of Appeal', 'SC': 'Supreme Court'}

      # Iterate over the dataframe of the result
      for idx, score1, score2 in result:
            # Get document from dataset using index
            current_doc = merged_dataset.iloc[idx]
            _score = round(score1*100)
            _court = current_doc["SUIT NUMBER"][:2]
            # Ignore results with less than 70% accuracy if there is no commonly cited case
            if _score < 70 and score2 == 0:
                  continue

            if _court in courts:
                  court = courts[_court]
            else:
                  court = None
            results.append({
                  "suit_number": current_doc["SUIT NUMBER"],
                  "subject_matter": current_doc["SUBJECT MATTER"],
                  "case_title": current_doc["CASE TITLE"],
                  "full_judgement": current_doc["FULL JUDGEMENT"],
                  "mutual_cited_cases": score2,
                  "court": court,
                  "percentage": _score
            })

      #sort result 
      results = sorted(results, key = lambda i: i['percentage'], reverse = True)

      #return result
      return {"data": results}

