import os
import json
from flask import Flask, g, jsonify, request, render_template, make_response, flash
from flask_cors import CORS, cross_origin
from werkzeug.utils import secure_filename

import preprocessdata as ped
from subapi import get_similar



app = Flask(__name__)
app.config.from_object('config')
cors = CORS(app)

PATH_TO_DATASET = app.config['PATH_TO_DATASET']

# Current file directory
here = os.path.dirname(__file__)


# Endpoints
@app.route("/")
def index():
    return ("Document similarity Fast Text Api")



# Generic text model with single text and cleaning process
@app.route('/similarities/generic-text', methods=['POST'])
@cross_origin()
def modelwithtext():
    text = request.form['content']
    clean_text = ped.clean_text(text)

    # Change to json format
    clean_text = {"content": clean_text}
    
    # Select model that applies
    model = os.path.join(here, 'fasttextModel')
    vec_list = os.path.join(here, 'vector list.pkl')
    result = get_similar(content=clean_text, vector_list=vec_list, full_text=text, model_file=model, PATH_TO_DATASET=PATH_TO_DATASET)
    return result



if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)
